A theme I built for myself because I like an older version of the Steam layout.
This is an unfinished version, it was posted early by request and I've been ocassionally tweaking things.

The correct aspect ratio for cover art is 171:80, that is what Steam actually rendered at by default. Playnite's preset is incorrect because it's based off of the source image instead of Steam. Crow refuses to fix this so you'll have to set the rendering ratio manually.

![](img/Img1.png)

![](img/Img2.png)

![](img/Img3.png)
